const { Transform, isErrored } = require('stream')
const { spawn } = require('child_process')

module.exports = class IOStream extends Transform {
  constructor (command, cmdArgs = [], options = {}) {
    if (!command) {
      throw new TypeError('first argument is mandatory')
    }
    if (typeof options !== 'object') {
      throw new TypeError('Options must be an object')
    }

    options = { io: {}, ...options }

    // throw new TypeError('First argument must be a string')
    if (!Array.isArray(cmdArgs)) {
      throw new TypeError('Second argument must be an Array')
    }

    super(options)

    this._io = {
      cp: null,
      command,
      cmdArgs,
      opts: {
        debug: false, // debugging output
        // success: 0, // TODO expected exit status code
        stderr: true, // whenever to throw if receives stderr data
        EOL: '\n', // End-Of-Line terminator for stderr message handling
        BOL: '\r', // Beggining-Of-Line terminator for stderr message handling
        ...options.io
      }
    }

    // const { command, args } = this._io
    const cp = this._io.cp = spawn(command, cmdArgs, {})
      .on('error', err => this.destroy(err))
      .on('spawn', () => this._debug('subprocess spawned'))
      .on('close', (code, signal) => {
        this._debug(`close with code ${code} and signal ${signal}`, true)

        // exit with status code 0 is the expected result
        if (code === 0 && !isErrored(this)) return this.push(null)

        /* if (cp.stdout.readableFlowing === null) {
          this._debug('stdout was not resumed')
          // cp.stdout.resume()
        } */

        // non-zero exit codes mean something unexpected
        if (std2.length) {
          console.error(std2.map(msg => `${this} stderr: ${msg}`).join('\n'))
        }
        this.destroy(new Error(`exit with code ${code} and signal ${signal}`))
      })

    cp.stdout
      /*
      .on('error', err => {
        console.error(err)
        this.destroy(new Error(`${this} stdout errored`))
      })
      */
      .on('readable', () => {
        this._debug('stdout source becomes readable')
        let read = true
        while (read) {
          const chunk = cp.stdout.read()
          this._debug(`stdout read ${chunk ? `${chunk.length} bytes` : chunk}`)

          // a null chunk will signal output's source EOF
          if (!chunk) break

          // continue reading as issued by back-pressure system
          read = this.push(chunk)
        }
      })
      .on('end', () => {
        this._debug('stdout source has end')
        // if (cp.exitCode === 0// this.end()
      })

    const { EOL, BOL } = this._io.opts
    const std2 = []
    let stderr = ''
    cp.stderr
      /*
      .on('error', err => {
        console.error(err)
        this.destroy(new Error(`${this} stderr errored`))
      })
      */
      .on('readable', () => {
        while (true) {
          const chunk = cp.stderr.read()
          // a null chunk will signal stderr's source EOF
          if (!chunk) break // {console.warn('stderr no chunk', chunk);break;}

          stderr += chunk.toString()

          if (stderr.indexOf(EOL) > -1) {
            stderr.split(EOL).forEach((msg, idx, messages) => {
              if (idx === messages.length - 1) {
                stderr = msg
                return
              }
              if (msg.indexOf(BOL) > -1) {
                return std2.push(msg.split(BOL).pop().trim())
              }
              std2.push(msg)
            })
          }
        }
      })
      .on('end', () => {
        if (!this._io.opts.stderr || stderr === '') return
        this.destroy(new Error(`${this} stderr: ${stderr}`))
      })

    cp.stdin
      .on('error', err => {
        this._debug(`stdin errored: ${err}`)
        process.nextTick(() => this.destroy(err))
      })
      .on('finish', () => {
        this._debug('stdin sink has finish', true)
      })
  }

  toString () {
    return `<IO ${this._io.command}>`
  }

  _transform (chunk, encoding, callback) {
    const { cp } = this._io
    this._debug(`${this} transform ${chunk.length} bytes`)

    if (typeof cp.exitCode === 'number') {
      return callback(new Error(`${this} has exit with code ${cp.exitCode}`))
    }

    if (!cp.stdin.writable) {
      return callback(new Error(`${this} stdin is not writable`))
    }

    try {
      if (cp.stdin.write(chunk, encoding)) {
        this._debug('synchronously finish transform')
        return callback()
      }
    } catch (err) {
      this.debug(`stdin write has errored: ${err}`)
      return callback(err)
    }

    this._debug('stdin sink write returned false, wait until it drains')
    cp.stdin.once('drain', () => {
      this._debug('stdin sink has drain')
      callback()
    })

    if (cp.stdout.isPaused()) {
      this._debug('resumed stdout source')
      cp.stdout.resume()
    }
  }

  _destroy (error, callback) {
    if (!error) {
      this._debug('destroying stream without errors')
      return callback()
    }

    const { cp } = this._io

    this._debug(`destroying stream beause ${error}...`, true)
    // console.error(error)

    if (!cp || cp.exitCode !== null) return callback(error)

    const signal = error.code === 'EPIPE' ? 'SIGPIPE' : 'SIGTERM'
    this._debug(`kill subprocess with ${signal}...`)

    cp.once('exit', (code, signal) => {
      this._debug(`exit affer killed with ${code} and ${signal}`)
      callback(error)
    })

    if (!cp.kill(signal)) {
      const err = new Error(`Could not kill ${this} subprocess`)
      err.killReason = error
      return callback(err)
    }
  }

  // /*
  _flush (callback) {
    const { cp } = this._io
    this._debug('flushing...', true)
    cp.stdin.end()
    cp.stdout.once('end', callback)
  }
  // */

  _debug (message, info = false) {
    if (!this._io.opts.debug) return

    const { cp } = this._io
    console.error(`${this} ${message}`, info
      ? {
          cpExitCode: cp.exitCode,
          cpKilled: cp.killed,
          // inputPending: cp.stdin.pending,
          // inputConnecting: cp.stdin.connecting,
          inputWritable: cp.stdin.writable,
          inputWritableEnded: cp.stdin.writableEnded,
          inputWritableLength: cp.stdin.writableLength,
          inputWritableFinished: cp.stdin.writableFinished,
          outputReadableEnded: cp.stdout.readableEnded,
          outputReadableLength: cp.stdout.readableLength,
          outputReadableFlowing: cp.stdout.readableFlowing,
          outputReadableisPaused: cp.stdout.isPaused(),
          ownWritableEnded: this.writableEnded,
          ownWritableFinished: this.writableFinished,
          ownReadableEnded: this.readableEnded,
          ownErrored: isErrored(this)
        }
      : ''
    )
  }
}

/* vim: set expandtab: */
/* vim: set filetype=javascript ts=2 shiftwidth=2: */
