Changelog

## [1.7.11](https://gitlab.com/iaigz/stream-io/compare/v1.7.10...v1.7.11) (2024-06-19)


### 🔧 Bug Fixes

* high severity CVE on braces package ([e8cc7fe](https://gitlab.com/iaigz/stream-io/commit/e8cc7fe0330c38b525f6dc7c05ddf879f80b9530))


### 💡 Development

* **deps:** update commitlint monorepo to v19.2.2 ([d67a064](https://gitlab.com/iaigz/stream-io/commit/d67a06482eb79308dffffd7d891311b0579fc82b))
* **deps:** update dependency @commitlint/cli to v19.3.0 ([482cd20](https://gitlab.com/iaigz/stream-io/commit/482cd200466afca4370de7c3a10fa596430f376e))
* **deps:** update dependency @semantic-release/gitlab to v13.0.4 ([f64f34c](https://gitlab.com/iaigz/stream-io/commit/f64f34c63683eb442df69854c0268d3a8c452c3a))
* **deps:** update dependency @semantic-release/gitlab to v13.1.0 ([be7abb3](https://gitlab.com/iaigz/stream-io/commit/be7abb38cea240d824aea42e7d4b6d48b3c4e975))
* **deps:** update dependency @semantic-release/npm to v12.0.1 ([c1c0729](https://gitlab.com/iaigz/stream-io/commit/c1c07299f25153679ed663045a918aa36dbb35f4))
* **deps:** update dependency semantic-release to v23.0.7 ([a526a1b](https://gitlab.com/iaigz/stream-io/commit/a526a1bde28036d950cf0e4fd51e9bd5d2f160a7))
* **deps:** update dependency semantic-release to v23.0.8 ([424b35d](https://gitlab.com/iaigz/stream-io/commit/424b35da048c1234ed8e859a25b067e6417f551c))
* **deps:** update dependency semantic-release to v23.1.1 ([1554982](https://gitlab.com/iaigz/stream-io/commit/1554982b7a18735ab61af08c90a80868965b7c78))
* **deps:** update node.js to >= 20.12.0 ([b6eb0f7](https://gitlab.com/iaigz/stream-io/commit/b6eb0f7e810da16f905471adb1f36887e950f2ef))
* **deps:** update node.js to >= 20.12.1 ([5661343](https://gitlab.com/iaigz/stream-io/commit/566134376f82fac65acb9d67a3334c71959567a7))
* **deps:** update node.js to >= 20.12.2 ([b9654ca](https://gitlab.com/iaigz/stream-io/commit/b9654ca0eaa7fea576a74dc13646908c84c5b8d7))
* **deps:** update node.js to >= 20.13.0 ([258bfa3](https://gitlab.com/iaigz/stream-io/commit/258bfa32d5ae3b0a00874a310c3d2ea5c3acf246))
* **deps:** update node.js to >= 20.13.1 ([ffc8500](https://gitlab.com/iaigz/stream-io/commit/ffc850094c2ea1a9ae3812eac75eec921db0616d))
* **deps:** update node.js to >= 20.14.0 ([2ebb221](https://gitlab.com/iaigz/stream-io/commit/2ebb2216dac0ce899c30805067affeb2792a6377))
* **deps:** update npm to >= 10.5.1 ([557c611](https://gitlab.com/iaigz/stream-io/commit/557c61153664e6123eed0fb27057cc86df53b673))
* **deps:** update npm to >= 10.5.2 ([4ed8dbf](https://gitlab.com/iaigz/stream-io/commit/4ed8dbf8a0646d38e7e219b52aa97c16ca917586))
* **deps:** update npm to >= 10.6.0 ([f0ecd7e](https://gitlab.com/iaigz/stream-io/commit/f0ecd7e35d88ec79ce46befe67db127f99867ed2))
* **deps:** update npm to >= 10.7.0 ([52d7091](https://gitlab.com/iaigz/stream-io/commit/52d709186f7e952ef119bd2afd3b781959b89610))
* **deps:** update npm to >= 10.8.0 ([7d58b93](https://gitlab.com/iaigz/stream-io/commit/7d58b9358ec30524a87de6654d2eab10a8b9c504))
* **deps:** update npm to >= 10.8.1 ([d89cd60](https://gitlab.com/iaigz/stream-io/commit/d89cd6059cb7b9af0e742916541388d1c2377b1b))

## [1.7.10](https://gitlab.com/iaigz/stream-io/compare/v1.7.9...v1.7.10) (2024-03-27)


### 💡 Development

* **deps:** update dependency semantic-release to v23.0.6 ([329e75f](https://gitlab.com/iaigz/stream-io/commit/329e75facc7644cebab009ae6948033165c01d54))

## [1.7.9](https://gitlab.com/iaigz/stream-io/compare/v1.7.8...v1.7.9) (2024-03-25)


### 💡 Development

* **deps:** update dependency @semantic-release/npm to v12 ([f7cd837](https://gitlab.com/iaigz/stream-io/commit/f7cd83703743bc8dd7a0151baee05e617c9636e9))

## [1.7.8](https://gitlab.com/iaigz/stream-io/compare/v1.7.7...v1.7.8) (2024-03-23)


### 💡 Development

* **deps:** migrate husky hook manager to v9 (major) ([7ccddd1](https://gitlab.com/iaigz/stream-io/commit/7ccddd1136076fa81ca93d513c60336b76148a6f))
* **deps:** update commitlint monorepo to v19 ([5fdc050](https://gitlab.com/iaigz/stream-io/commit/5fdc050e2396636a4c3cb9ca67d4beac37597bf2))
* **deps:** update dependency @commitlint/cli to v19.2.1 ([0602e6e](https://gitlab.com/iaigz/stream-io/commit/0602e6e3e51818b789693269b0376db5ba36e161))
* **deps:** update dependency husky to v9 ([83c1fa5](https://gitlab.com/iaigz/stream-io/commit/83c1fa5bb0e46faa2c4638feb4fb61408f075442))
* **deps:** update dependency semantic-release to v23.0.3 ([d5977f4](https://gitlab.com/iaigz/stream-io/commit/d5977f4004b1007a738d792591c109291b264c0a))
* **deps:** update dependency semantic-release to v23.0.4 ([e57784f](https://gitlab.com/iaigz/stream-io/commit/e57784f39e1a5b4dd6892d2863ebc997c83966f4))
* **deps:** update dependency semantic-release to v23.0.5 ([d1d7e60](https://gitlab.com/iaigz/stream-io/commit/d1d7e60c99f0faab5a3159e3da2c9eda6efe93ea))

## [1.7.7](https://gitlab.com/iaigz/stream-io/compare/v1.7.6...v1.7.7) (2024-03-16)

## [1.7.6](https://gitlab.com/iaigz/stream-io/compare/v1.7.5...v1.7.6) (2024-03-15)


### 💡 Development

* **deps:** update commitlint monorepo ([4976ac6](https://gitlab.com/iaigz/stream-io/commit/4976ac6badd7ba8c44ff58a1fa6f34d40492662e))
* **deps:** update dependency @commitlint/config-conventional to v18.6.3 ([86d024a](https://gitlab.com/iaigz/stream-io/commit/86d024a553777d1bcc16ca1779dda4060b53e751))
* **deps:** update dependency @semantic-release/gitlab to v13.0.3 ([c88526b](https://gitlab.com/iaigz/stream-io/commit/c88526bd8d994e7bd6b1a4f095981e30abf78f91))
* **deps:** update dependency @semantic-release/npm to v11.0.3 ([6ca671f](https://gitlab.com/iaigz/stream-io/commit/6ca671f57dfe8380a03ab2b864573497015adab3))
* **deps:** update node.js to >= 20.11.0 ([2dedf99](https://gitlab.com/iaigz/stream-io/commit/2dedf995e4fe651b0e301105e337a3ef6fce1774))
* **deps:** update node.js to >= 20.11.1 ([d10b914](https://gitlab.com/iaigz/stream-io/commit/d10b914d4ca51e9640ac31748e1d7d448e89dc5d))
* **deps:** update npm to >= 10.5.0 ([7e247ab](https://gitlab.com/iaigz/stream-io/commit/7e247ab202fe0097021a0362b77f5e3dc5af0553))

## [1.7.5](https://gitlab.com/iaigz/stream-io/compare/v1.7.4...v1.7.5) (2024-02-10)


### 💡 Development

* **deps:** update dependency semantic-release to v23.0.2 ([2bfae85](https://gitlab.com/iaigz/stream-io/commit/2bfae856ac75eb063354dc2db85f12854fd711d6))

## [1.7.4](https://gitlab.com/iaigz/stream-io/compare/v1.7.3...v1.7.4) (2024-01-29)


### 💡 Development

* **deps:** update commitlint monorepo to v18.6.0 ([7642187](https://gitlab.com/iaigz/stream-io/commit/7642187e01c3e6e9084be6671042f3e5c619b050))
* **deps:** update npm to >= 10.4.0 ([298963a](https://gitlab.com/iaigz/stream-io/commit/298963aaf6e4fff13931ea067dfac59dfed0316d))

## [1.7.3](https://gitlab.com/iaigz/stream-io/compare/v1.7.2...v1.7.3) (2024-01-25)


### 💡 Development

* **deps:** update commitlint monorepo to v18.5.0 ([0a94b93](https://gitlab.com/iaigz/stream-io/commit/0a94b93767f86d97324e22a5d75d78b0a477c5e5))

## [1.7.2](https://gitlab.com/iaigz/stream-io/compare/v1.7.1...v1.7.2) (2024-01-20)


### 💡 Development

* **deps:** update dependency semantic-release to v23 ([d323979](https://gitlab.com/iaigz/stream-io/commit/d32397970f09bb85def3debc17d287d2c3e45b63))

## [1.7.1](https://gitlab.com/iaigz/stream-io/compare/v1.7.0...v1.7.1) (2024-01-13)


### 💡 Development

* **deps:** update npm to >= 10.3.0 ([43936e2](https://gitlab.com/iaigz/stream-io/commit/43936e2c64186b11edf7f272d6e72943f3a1607c))

## [1.7.0](https://gitlab.com/iaigz/stream-io/compare/v1.6.20...v1.7.0) (2024-01-12)


### 🚀 Features

* **README:** note about stream.pipeline, trigger a release ([a9d6b27](https://gitlab.com/iaigz/stream-io/commit/a9d6b275b504f6bcb5bfd3591c47eba61c7b7d4e))

## [1.6.20](https://gitlab.com/iaigz/stream-io/compare/v1.6.19...v1.6.20) (2024-01-12)


### 🔧 Bug Fixes

* **release config:** found issue with @semantic-release/git documentation ([9961c7e](https://gitlab.com/iaigz/stream-io/commit/9961c7ebbfafbb2f7a5af4e1076a126c11fb1782))

## [1.6.19](https://gitlab.com/iaigz/stream-io/compare/v1.6.18...v1.6.19) (2024-01-12)


### 🔧 Bug Fixes

* **package.json:** normalize repository url with npm pkg fix ([44828e8](https://gitlab.com/iaigz/stream-io/commit/44828e8204e288619e29aa1cd247ac735780679e))

## [1.6.18](https://gitlab.com/iaigz/stream-io/compare/v1.6.17...v1.6.18) (2024-01-12)


### 💡 Development

* **release config:** don't skip CI when releasing on next to allow gitops workflow automation ([79601ea](https://gitlab.com/iaigz/stream-io/commit/79601eafc5a7a33382609fec18dd2e7fcbbdd321))

## [1.6.17](https://gitlab.com/iaigz/stream-io/compare/v1.6.16...v1.6.17) (2024-01-12)


### 💡 Development

* **deps:** update dependency @semantic-release/gitlab to v13 ([d6f08df](https://gitlab.com/iaigz/stream-io/commit/d6f08df0085056c641b8eeb5505c802a395e1abc))

## [1.6.16](https://gitlab.com/iaigz/stream-io/compare/v1.6.15...v1.6.16) (2024-01-12)


### 💡 Development

* **deps:** update node.js to 8e6a472 ([8f4a87a](https://gitlab.com/iaigz/stream-io/commit/8f4a87abd438a9ba3362c1d06240ba1d59eb8c79))

## [1.6.15](https://gitlab.com/iaigz/stream-io/compare/v1.6.14...v1.6.15) (2024-01-07)


### 💡 Development

* **deps:** update commitlint monorepo to v18.4.4 ([3691a72](https://gitlab.com/iaigz/stream-io/commit/3691a7289f3657f8de7da56ac9e45af1426d71b2))

## [1.6.14](https://gitlab.com/iaigz/stream-io/compare/v1.6.13...v1.6.14) (2023-12-27)


### 💡 Development

* reduce complexity of _destroy implementation ([b28f7ef](https://gitlab.com/iaigz/stream-io/commit/b28f7ef1ec0fc9cbceb827cebbedee2958b52ba9))
* **test:** wrap output following autotest guidelines ([ac77971](https://gitlab.com/iaigz/stream-io/commit/ac77971444989d63a012454fbe8cff89ba331195))

## [1.6.13](https://gitlab.com/iaigz/stream-io/compare/v1.6.12...v1.6.13) (2023-12-23)


### 💡 Development

* **deps:** update node.js to v20 ([6375cb7](https://gitlab.com/iaigz/stream-io/commit/6375cb7cf4047248a5221aec9de34ce4e07c6322))

## [1.6.12](https://gitlab.com/iaigz/stream-io/compare/v1.6.11...v1.6.12) (2023-12-15)


### 💡 Development

* **deps:** update dependency semantic-release to v22.0.12 ([7be6b0c](https://gitlab.com/iaigz/stream-io/commit/7be6b0cb87b9d4a4a8c9596fb6243d816ed46420))

## [1.6.11](https://gitlab.com/iaigz/stream-io/compare/v1.6.10...v1.6.11) (2023-12-13)


### 💡 Development

* Merge 'renovate/npm-10.x' into 'next' ([56a8692](https://gitlab.com/iaigz/stream-io/commit/56a86924cf92378448d6f9b48689eba1ca6f9c5f))
* Merge 'renovate/semantic-release-monorepo' into 'next' ([1fc850f](https://gitlab.com/iaigz/stream-io/commit/1fc850fd50fd43a5d231416c9be8aae94b6008f4))
* **deps:** update dependency semantic-release to v22.0.9 ([c1d3a78](https://gitlab.com/iaigz/stream-io/commit/c1d3a783b4b2ca4aa3b6c03d957cf7620f2ab1e0))
* **deps:** update npm to >= 10.2.5 ([cec52b1](https://gitlab.com/iaigz/stream-io/commit/cec52b1d134015e36c903c01f2da500258d0dbea))
* **deps:** update semantic-release monorepo ([167a693](https://gitlab.com/iaigz/stream-io/commit/167a69388cf369eca03d9b640844958f52aab123))

## [1.6.10](https://gitlab.com/iaigz/stream-io/compare/v1.6.9...v1.6.10) (2023-12-05)


### 💡 Development

* **deps:** update npm to >= 10.2.4 ([c3959c9](https://gitlab.com/iaigz/stream-io/commit/c3959c9445e2c797efdac5782d68daeaae71823f))

## [1.6.9](https://gitlab.com/iaigz/stream-io/compare/v1.6.8...v1.6.9) (2023-12-04)


### 💡 Development

* **deps:** update semantic-release monorepo ([471b45f](https://gitlab.com/iaigz/stream-io/commit/471b45fd0d5e1ef0f0ba4f4daecfc74c9445d358))

## [1.6.8](https://gitlab.com/iaigz/stream-io/compare/v1.6.7...v1.6.8) (2023-12-02)


### 💡 Development

* Merge 'renovate/major-commitlint-monorepo' into 'next' ([a85a785](https://gitlab.com/iaigz/stream-io/commit/a85a785271dce0639d6db8b519e3350c635685c7))
* Merge 'renovate/node-18.x' into 'next' ([a2d7606](https://gitlab.com/iaigz/stream-io/commit/a2d760663c3e6af5ad655bc39fe4842684304647))
* **deps:** update commitlint monorepo to v18 ([b2fe2c8](https://gitlab.com/iaigz/stream-io/commit/b2fe2c87a63558d1790ff0511505ce86be8bb0df))
* **deps:** update node.js to >= 18.19.0 ([8aee684](https://gitlab.com/iaigz/stream-io/commit/8aee68424f584453e40c367f4e61ab0e64d602c4))

## [1.6.7](https://gitlab.com/iaigz/stream-io/compare/v1.6.6...v1.6.7) (2023-12-02)


### 💡 Development

* Merge 'renovate/configure' into 'next' ([e113bd4](https://gitlab.com/iaigz/stream-io/commit/e113bd485535105075fe47e35ea4c9f3c7c58a2e))
* Merge 'renovate/npm-9.x' into 'next' ([6b5a158](https://gitlab.com/iaigz/stream-io/commit/6b5a15826067411c28d1ee9c2f3aa697c1611560))
* Merge 'renovate/pin-dependencies' into 'next' ([f3998f9](https://gitlab.com/iaigz/stream-io/commit/f3998f919df0334b1ca8e909665ffe9ca8b34e24))
* **deps:** pin dependencies ([457cbec](https://gitlab.com/iaigz/stream-io/commit/457cbecd5fbe7dfc07b0f68dd2deb366b3acaa52))
* **deps:** update commitlint monorepo to v17.8.1 ([58d22eb](https://gitlab.com/iaigz/stream-io/commit/58d22eba4e5561a767841976a4ee9dfa9be54b32))
* **deps:** update node.js to >= 18.18.2 ([774b6d8](https://gitlab.com/iaigz/stream-io/commit/774b6d8dea13b3417e7f9982e9c6cda49a21a245))
* **deps:** update npm to >= 9.9.2 ([f73f6e8](https://gitlab.com/iaigz/stream-io/commit/f73f6e8017fc56e39e1fb73c20888878a73b7447))
* **deps:** update semantic-release monorepo ([4009660](https://gitlab.com/iaigz/stream-io/commit/4009660d00002d15d749d08e37ced80c616f0431))

## [1.6.6](https://gitlab.com/iaigz/stream-io/compare/v1.6.5...v1.6.6) (2023-11-11)


### 📚 Documentation

* **README:** fix typo, aprovecho para ver cómo está el CICD ([3b304b5](https://gitlab.com/iaigz/stream-io/commit/3b304b5fe40054117740e2ac5d9eb41ffe021ab7))

## [1.6.5](https://gitlab.com/iaigz/stream-io/compare/v1.6.4...v1.6.5) (2023-10-05)


### 💡 Development

* **release config:** adjust types and notes generator mergePattern ([e53b8bc](https://gitlab.com/iaigz/stream-io/commit/e53b8bcd2368303d02b823737c07758dd0936e1f))

## [1.6.4](https://gitlab.com/iaigz/stream-io/compare/v1.6.3...v1.6.4) (2023-09-12)

## [1.6.3](https://gitlab.com/iaigz/stream-io/compare/v1.6.2...v1.6.3) (2023-09-12)

## [1.6.2](https://gitlab.com/iaigz/stream-io/compare/v1.6.1...v1.6.2) (2023-09-12)


### 💡 Development

* **release:** new format for merge commits ([8b0050c](https://gitlab.com/iaigz/stream-io/commit/8b0050c93f91659171694fbb72b81d17015cb2cc))

## [1.6.1](https://gitlab.com/iaigz/stream-io/compare/v1.6.0...v1.6.1) (2023-09-12)

## [1.6.0](https://gitlab.com/iaigz/stream-io/compare/v1.5.0...v1.6.0) (2023-09-12)


### 🚀 Features

* **release:** detect merge commits ([1f274d5](https://gitlab.com/iaigz/stream-io/commit/1f274d5ceb026161e328866ee8c06a52856c6c8d))

## [1.5.0](https://gitlab.com/iaigz/stream-io/compare/v1.4.0...v1.5.0) (2023-09-12)


### 🚀 Features

* **CHANGELOG:** sections for commit types ([e991304](https://gitlab.com/iaigz/stream-io/commit/e991304692f5ac2dab6214a125251f26b3ccd2a1))
* **release:** release patches for perf commits ([50ea5b0](https://gitlab.com/iaigz/stream-io/commit/50ea5b00c633c11bc902c8f70bae40105dd9c1d3))
* **release:** signal a feature to trigger release mechanics ([afb8ce5](https://gitlab.com/iaigz/stream-io/commit/afb8ce5ec1f3806ac8fa23222a3d24ab0499ee4c))


### 🔧 Bug Fixes

* **release:** config file ([30f2e7a](https://gitlab.com/iaigz/stream-io/commit/30f2e7aad888f9f59cf39c15e0650e94d8744d63))
* **release:** move presetConfig to top-level scope ([9d2166d](https://gitlab.com/iaigz/stream-io/commit/9d2166d6b0400d42816db61f180cf3931ac6fd9a))


### 💡 Development

* **deps:** re-install conventionalcommits preset ([b428b3b](https://gitlab.com/iaigz/stream-io/commit/b428b3b52722ba05c5d28ae8b344a3991c42ca9a))
* **deps:** remove conventional-changelog-conventionalcommits ([28c2ce3](https://gitlab.com/iaigz/stream-io/commit/28c2ce348125c0726be8fc11d52851af7359a3cf))
* **release config:** test without presetConfig set ([fc6e285](https://gitlab.com/iaigz/stream-io/commit/fc6e285e63e9475e7f02ddb6bc087182705140ce))
* **release:** disable presetConfig completelly ([35979fe](https://gitlab.com/iaigz/stream-io/commit/35979fe039dd52a13addd1a82dcc47bbdad39733))
* **release:** disable presetConfig:preset on release notes generator ([90bc4cb](https://gitlab.com/iaigz/stream-io/commit/90bc4cb19c207db2c5cf2a5f985fce1d9cd07091))
* **release:** simplify config ([6a64d34](https://gitlab.com/iaigz/stream-io/commit/6a64d3483b6047bb509ed21944602fe9ed46447e))

## [1.4.0](https://gitlab.com/iaigz/stream-io/compare/v1.3.0...v1.4.0) (2023-09-12)


### Features

* **release:** commit changes to package.json ([ff6ca93](https://gitlab.com/iaigz/stream-io/commit/ff6ca93298edc796749433eee66ceaff5a01b4b6))

# Changelog

## [1.3.0](https://gitlab.com/iaigz/stream-io/compare/v1.2.1...v1.3.0) (2023-09-12)


### Features

* **release:** generate a changelog file, commit changes to git repository ([ddc5c5b](https://gitlab.com/iaigz/stream-io/commit/ddc5c5bf876084b26a68387a9165ac5e2703bfaa))
